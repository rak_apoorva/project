#include "neuralnet.h"
#include "header.h"

NNetwork::NNetwork()
{
    srand(time(0));
    cout<<"Loading Config Parameters"<<endl;
    loadconfig();
    cout<<"Loaded"<<endl;
    cout<<"Load I/O File"<<endl;
    loadIOfile();
    cout<<"Loaded"<<endl;
    cout<<"Initialize Network"<<endl;
    neuNetwork = new nNet;
    cout<<"Done"<<endl;
    cout<<"Building Input Layers"<<endl;
    buildInLayer();
    cout<<"Built"<<endl;
    cout<<"Building Hidden Layers"<<endl;
    buildHidLayer();
    cout<<"Built"<<endl;
    cout<<"Building Output Layers"<<endl;
    buildOutLayer();
    cout<<"Built"<<endl;
    testparam();

}

void NNetwork::loadconfig()
{
    fstream config;
    int r=0;
    string line;
    string line1,line2,line3,line4,line5,line6,line7,line8;
    config.open("D:\\Books\\Programming\\Assignments\\Project\\config.txt");
    while(!config.eof())// && r<=PARA )
    {
        config>>line;
        on=atof(line.c_str());
        config>>line;
        off=atof(line.c_str());
        config>>line;
        inunits=atoi(line.c_str())+1;
        config>>line;
        hidunits=atoi(line.c_str())+1;
        config>>line;
        outunits=atoi(line.c_str());
        config>>line;
        maxepoch=atoi(line.c_str());
        config>>line;
        learnrate=atof(line.c_str());
        config>>line;
        ee=atof(line.c_str());

    }
    config.close();
}
void NNetwork::loadIOfile()
{
    inputdata=new float*[4];
    outputdata=new float*[4];
    string data1,data2,data3;
    int lines=0;
    fstream traindata;
    traindata.open("D:\\Books\\Programming\\Assignments\\Project\\traindata.txt");
    if(traindata.is_open())
    {

       for (int i=0;i<4;i++)
       {
           inputdata[i]=new float[2];
       }
        for (int i=0;i<4;i++)
        {
            for (int j=0;j<2;j++)
            {
                traindata>>inputdata[i][j];
            }
            lines++;
        }
        for (int i=0;i<4;i++)
        {
            outputdata[i]=new float[1];
        }
        for (int i=0;i<4;i++)
        {
            for (int j=0;j<1;j++)
            {
                traindata>>outputdata[i][j];
            }
            lines++;
        }
        iopairs =lines/2;
        traindata.close();
    }



}
float NNetwork::randweight()
{


    return ((float(rand()) / float(RAND_MAX)) * (1.0 - -1.0)) + -1.0;


}

void NNetwork::train()
{

    for (int ep=0;ep<maxepoch;ep++)
    {
        for (int i=0;i<iopairs;i++)
        {

            assignactivation(i);
            propagateact();
            computerror(i);
            backpropagate();
        }
    }
}
void NNetwork::test()
{

    cout<<"Output Neuron"<<endl;
    for (int i=0;i<iopairs;i++)
    {
        assignactivation(i);
        propagateact();
        computerror(i);
        cout<<neuNetwork->outputlayer.x[i]<<endl;


    }

}



void NNetwork::assignactivation(int r)
{
    for(int i=0;i<inunits-1;i++)
    {
        neuNetwork->inputlayer.x[i]=inputdata[r][i];

    }

}

void NNetwork::propagateact()
{
    int i, j;
    //input-hidden
    double temp=0;
    for (i=0;i<hidunits-1;i++)
    {
        for (j=0;j<inunits;j++)
        {
            temp=temp+(neuNetwork->inputlayer.x[j]*neuNetwork->inputlayer.w[j][i]);

        }
      neuNetwork->hiddenlayer.x[i]=((1.0) / (1.0 + pow(ee,-temp)));

        temp=0.0;
    }

    //hidden-output
    for (i = 0; i < outunits; i++)
    {
        for (j = 0; j < hidunits; j++)
        {
            temp=temp+(neuNetwork->hiddenlayer.x[j]*neuNetwork->hiddenlayer.w[j][i]);
        }
        neuNetwork->outputlayer.x[i]=((1.0) / (1.0 + pow(ee,-temp)));
        temp=0.0;
    }


}

void NNetwork::computerror(int r)
{
    ofstream mserror;
    mserror.open("D:\\Books\\Programming\\Assignments\\Project\\mse.txt");
    int i,j;
    float sum;
    float temp=0;
    float error=0;
    for(i=0;i<outunits;i++)
    {
        neuNetwork->outputlayer.err[i]=(neuNetwork->outputlayer.x[i]*(1.0-neuNetwork->outputlayer.x[i])*
                                       (outputdata[r][i]-neuNetwork->outputlayer.x[i]));
    }

    for(i=0;i<outunits;i++)
    {
        error=error+outputdata[r][i]-neuNetwork->outputlayer.x[i];

    }
    temp=pow(error,2);
    mse=temp;
   // mserror<<mse<<endl;

    for(j=0;j<hidunits;j++)
    {
        for(i=0;i<outunits;i++)
        {
            sum=sum+neuNetwork->outputlayer.err[i]*neuNetwork->hiddenlayer.w[j][i];
        }
        neuNetwork->hiddenlayer.err[j]=
                (neuNetwork->hiddenlayer.x[j]*
                (1-neuNetwork->hiddenlayer.x[j]))*sum;
    }

}

void NNetwork::backpropagate()
{
    int i,j;
    for(i=0;i<hidunits;i++)
    {
        for(j=0;j<outunits;j++)
        {
            neuNetwork->hiddenlayer.w[i][j]=neuNetwork->hiddenlayer.w[i][j]+
                    (learnrate*neuNetwork->outputlayer.err[j]*neuNetwork->hiddenlayer.x[i]);
        }
    }
    for(i=0;i<inunits;i++)
    {
        for(j=0;j<hidunits-1;j++)
        {
            neuNetwork->inputlayer.w[i][j]=neuNetwork->inputlayer.w[i][j]+
                    (learnrate*neuNetwork->hiddenlayer.err[j]*neuNetwork->inputlayer.x[i]);
        }
    }
}


void NNetwork::buildInLayer()
{
    neuNetwork->inputlayer.x=new float[inunits];
    neuNetwork->inputlayer.w=new float*[inunits];

    for (int i = 0; i <inunits; i++)
    {
        neuNetwork->inputlayer.w[i]=new float[hidunits-1];

    }
    for (int i=0;i<inunits;i++)
    {
        for(int j=0;j<hidunits-1;j++)
        {

            neuNetwork->inputlayer.w[i][j]=randweight();


        }
    }
}

void NNetwork::buildHidLayer()
{
    neuNetwork->hiddenlayer.x=new float[hidunits];
    neuNetwork->hiddenlayer.w=new float*[hidunits];
    neuNetwork->hiddenlayer.err=new float[hidunits];
    for (int i = 0; i<hidunits; i++)
    {
        neuNetwork->hiddenlayer.w[i]=new float[outunits];

    }

    for (int i=0;i<hidunits;i++)
    {
        for(int j=0;j<outunits;j++)
        {
            neuNetwork->hiddenlayer.w[i][j]=randweight();
        }
    }

}

void NNetwork::buildOutLayer()
{
    neuNetwork->outputlayer.x=new float[outunits];
    neuNetwork->outputlayer.err=new float[outunits];

}

void NNetwork::saveweights()
{
    int i,j;
    ofstream output;
    output.open("D:\\Books\\Programming\\Assignments\\Project\\weights.txt");
    if(!output)
    {
        cout<<"Failed to open file"<<endl;
    }
    for (i = 0; i < inunits; i++)
    {
        for (j = 0; j < hidunits+1; j++)
        {
            output<<neuNetwork->inputlayer.w[i][j]<<endl;
        }
    }
    for (i = 0; i < hidunits; i++)
    {
        for (j = 0; j < outunits+1; j++)
        {
            output<<neuNetwork->hiddenlayer.w[i][j]<<endl;
        }
    }
    output.close();

}
void NNetwork::testparam()
{
    cout<<"Default Parameters"<<endl;
    cout<<on<<endl<<off<<endl<<inunits<<endl<<hidunits<<endl<<outunits<<endl<<iopairs<<endl<<maxepoch<<endl<<learnrate<<endl<<ee<<endl;
    cout<<"Input data"<<endl;
    for (int i=0;i<4;i++)
    {
        for (int j=0;j<2;j++)
        {
            cout<<inputdata[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<"Output data"<<endl;
    for (int i=0;i<4;i++)
    {
        for (int j = 0; j < 1; j++)
        {
            cout << outputdata[i][j] << endl;
        }
    }
    //cout<<"Test Output"<<endl;


}


