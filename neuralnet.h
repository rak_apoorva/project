#ifndef PROJECT_NEURALNET_H
#define PROJECT_NEURALNET_H



class NNetwork
{
public:
    NNetwork();
  //  ~NNetwork();

    void train();
    void test();
    void saveweights();
    void loadweights();
private:
    int inunits,hidunits,outunits,iopairs,maxepoch;
    float on,off;
    float learnrate;
    double ee;
    float mse;

    float **outputdata;
    float **inputdata;

    struct inlayer
    {
        float *x;
        float **w;
    };
    struct hidlayer
    {
        float *x;
        float **w;
        float *err;
    };
    struct outlayer
    {
        float *x;
        float *err;
    };
    struct nNet
    {
        inlayer inputlayer;
        hidlayer hiddenlayer;
        outlayer outputlayer;
    };
    nNet *neuNetwork;

    float randweight();
    void loadconfig();
    void buildInLayer();
    void buildHidLayer();
    void buildOutLayer();


    void buildIOdata();
    void loadIOfile();

    void assignactivation(int);
    void propagateact();
    void computerror(int);
    void backpropagate();
    void testparam();

};


#endif
